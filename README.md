# EVE Timerboard

A barebones timerboard written with the Django web framework.

This isn't setup super well for others to develop, but instructions on how to setup a dev instance are included.

This doesn't track notifications from in-game, you have to manually add anytimers.

## Requirements

You will need to have:
- Python3
- Docker

# How to deploy

## Setup env files
### For production:
1) Rename the env.setup to .env.prod, and env.setup.db to .env.prod.db

2) Go-to https://developers.eveonline.com, sign in and create a new application.

    - Select `Authentication Only`
    - Set your callback URL to https://hostname/accounts/eveonline/login/callback
    - Copy the `Client ID` and `Secret Key` and put those as the values for `client_id` and `secret_key` in your .env file.

3) Generate a secret key for Django

    - You can do this by running
        
        ```python -c "import secrets; print(secrets.token_urlsafe())```
    
    set the SECRET_KEY value to this.

4) Set the other variables

    - DJANGO_ALLOWED_HOSTS should include the host you're running on, i.e `timerboard.example`
    - SQL_USER, SQL_PASSWORD, and SQL_DATABASE should be the same as the `POSTGRES_USER`, `POSTGRES_PASSWORD`, and `POSTGRES_DB` in .env.prod.db
    - The dockerfiles assume postgres, if you're running something else make sure to make the proper adjustments.

5) To run do:

``` 
# Build containers
docker-compose -f docker-compose.prod.yml up -d --build

# Migrate to setup models in the DB
docker-compose exec web python manage.py migrate --noinput

# Setup crontab on host to check and remove old timers every 5 minutes
sudo (crontab -l && echo "*/5 * * * * docker-compose -f docker-compose.prod.yml exec web python manage.py remove_old_timers") | crontab -
```

### For dev

1) Follow the same steps as for production but rename the env.setup to .env.dev
2) Set the `DJANGO_ALLOWED_HOSTS` variable to `localhost 127.0.0.1 [::1]`
3) Set `DEBUG` to `True` or `1`
4) To run do:
```
docker-compose -f docker-compose.yml up -d --build
```
The entrypoint.sh script will handle the migrations.  Note that this will wipe the DB everytime you rebuild the containers.









