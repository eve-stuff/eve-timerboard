from datetime import datetime, timedelta
from pyexpat.errors import messages
from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpRequest
from .forms import TimerForm
from .models import Timer
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth.decorators import login_required, permission_required
from .decorators import user_is_verified
from .clean import *

# Create your views here.

def add_timer(request):
    if request.method == 'POST':
        form = TimerForm(request.POST)
        if form.is_valid():
            location, name, time = translate_timer(str(form.cleaned_data['timer']))
            Timer(
                name = name,
                stage = form.cleaned_data['stage'],
                type = form.cleaned_data['type'],
                location = location,
                time = time,
                owner = form.cleaned_data['owner'],

                user=request.user,
            ).save()
            messages.success(request, 'Successfully added timer')
            return redirect('timerboard-view')
        else:
            
            messages.error(request, 'Failed to add timer')
            return redirect('timerboard-view')

def remove_timer(request, pk):
    Timer.objects.get(pk=pk).delete()
    return redirect('timerboard-view')

@login_required
@user_is_verified
def view_timerboard(request):
    timers = Timer.objects.filter(time__gte=timezone.now())
    timers = Timer.objects.order_by('time')
    context = {
        'timers': timers,
        'form': TimerForm()
    }
    return render(request, 'timerboard/timerboard.html', context)

def view_homepage(request):
    return render(request, 'timerboard/home.html')