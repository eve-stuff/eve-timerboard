import re
from datetime import datetime

def translate_timer(input):
    print(input)
    if '<br>' in input:
        input = input.replace('<br>','')
        match = re.search(r'(?P<sys>.+)\s\-\s(?P<name>.+)Distance.+until\s(?P<year>\d+)\.(?P<month>\d+)\.(?P<day>\d+)\s+(?P<hour>\d+):(?P<min>\d+):(?P<sec>\d+)', input)
    else:
        match = re.search(r'(?P<sys>.+)\s\-\s(?P<name>.+)\s\d.+until\s(?P<year>\d+)\.(?P<month>\d+)\.(?P<day>\d+)\s+(?P<hour>\d+):(?P<min>\d+):(?P<sec>\d+)', input)

    location = match.group('sys')
    name = match.group('name')
    match = re.search(r'\d{4}.\d{2}.\d{2} \d{2}:\d{2}:\d{2}', input)
    date = datetime.strptime(match.group(), '%Y.%m.%d %H:%M:%S')
    return location, name, date