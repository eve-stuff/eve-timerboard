from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_timerboard, name='timerboard-view'),
    path('timerboard/addtimer', views.add_timer, name="timerboard-add"),
    path('timerboard/removetimer/<int:pk>/', views.remove_timer, name="timerboard-remove"),
    path('timerboard/home', views.view_homepage, name="timerboard-login"),
]