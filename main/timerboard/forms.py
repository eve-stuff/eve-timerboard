from django import forms
from .models import Timer

class TimerForm(forms.Form):
    timer = forms.CharField(required=True)
    type = forms.CharField(max_length=64, required=True)
    stage = forms.CharField(max_length=64, required=True)
    owner = forms.CharField(max_length=64, required=True)

