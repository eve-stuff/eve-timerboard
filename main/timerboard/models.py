from django.db import models
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialAccount
from django.db.models.functions import Now
import requests
from .whitelist import *
# Create your models here.

class TimerManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(time__lte=Now())

class Timer(models.Model):
    '''
        Data model for storing timers
        name = name of structure
        stage = shield/armor/structure
        type = offensive/defensive
        location = where the timer is
        time = time left till it comes out
        user = who added the timer
    '''
    name = models.CharField(max_length=64)
    stage = models.CharField(max_length=64)
    type = models.CharField(max_length=64)
    location = models.CharField(max_length=64)
    time = models.DateTimeField(auto_now=False, auto_now_add=False)
    owner = models.CharField(max_length=64)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    #objects = TimerManager()

class TimerBoardUser(models.Model):

    user = models.OneToOneField(User, related_name='profile', null=True, on_delete=models.SET_NULL)
    def __str__(self):
        return "{}'s profile".format(self.user.username)
    
    class Meta:
        db_table = 'user_profile'

    def get_char_id(self):
        char_id = SocialAccount.objects.filter(user_id=self.user.id, provider='eveonline')
        
        return char_id[0].extra_data.get("CharacterID", 1)
    
    def get_alliance_id(self):
        char_id = self.get_char_id()
        r = requests.get(url = f"https://esi.evetech.net/latest/characters/{char_id}")
        data = r.json()
        if 'alliance_id' in data:
            alliance_id = data['alliance_id']
        else:
            alliance_id=''
        return alliance_id
    
    def account_verified(self):
        if self.user.is_authenticated:
            ally_id = str(self.get_alliance_id())
            if len(str(ally_id)) != 0:
                if ally_id in ALLIANCES:
                    
                    return True
                else:
                    
                    return False
            else:
                return False


    
User.profile = property(lambda u: TimerBoardUser.objects.get_or_create(user=u)[0])
    
    # TODO:// Implement check to make sure a user is in the approved alliance list
