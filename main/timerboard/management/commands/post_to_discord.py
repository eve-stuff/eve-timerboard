import requests
import os
from django.core.management.base import BaseCommand
from django.db.models.functions import Now
from timerboard.models import Timer
from django.forms.models import model_to_dict
from datetime import timedelta, datetime
from django.utils import timezone

class Command(BaseCommand):
    help = "Posts upcoming timers to discord"

    def handle(self, *args, **options):
        url = os.environ.get("DISC_WEBHOOK")
        timers_now = Timer._base_manager.filter(time__gte=Now()+timedelta(minutes=-30), time__lte=Now())
        timers_tom = Timer._base_manager.filter(time__gte=Now()+timedelta(hours=23, minutes=55), time__lte=Now()+timedelta(days=1))
        for time in timers_now:
            data = {
                "content": "@here",
                "username" : "Ryan Renold's Fan Club",
                "avatar_url" : "https://hips.hearstapps.com/digitalspyuk.cdnds.net/18/07/1518696163-rr.jpg?crop=0.754xw:0.510xh;0.109xw,0.00936xh&resize=480:*"
            }   
            data["embeds"] = [{
                    "title" : "Timer comes out now",
                    "thumbnail":{
                        "url":"https://images.heb.com/is/image/HEBGrocery/000402171",
                    },
                    "fields" : [{
                        "name": "Name",
                        "value": time.name,
                    },
                    {
                        "name": "Location",
                        "value": time.location,
                    },
                    {
                        "name": "Type",
                        "value": time.type,
                        "inline": True
                    },
                    {
                        "name": "Owner",
                        "value": time.owner,
                        "inline": True
                    },
                    {
                        "name": "Stage",
                        "value": time.stage,
                        "inline": True
                    },
                    {
                        "name": "Timer",
                        "value": str(time.time.replace(tzinfo=None)),
                        "inline": True
                    }
                ]
            }]
            request = requests.post(url, json=data)
            print(request)
            print(time.name, time.location, time.stage, time.type, time.owner, time.time)

        for time in timers_tom:
            print(time)
            data = {
                "content": "@here",
                "username" : "Ryan Renold's Fan Club",
                "avatar_url": "https://hips.hearstapps.com/digitalspyuk.cdnds.net/18/07/1518696163-rr.jpg?crop=0.754xw:0.510xh;0.109xw,0.00936xh&resize=480:*",
            }   
            data["embeds"] = [{
                    "title" : "Timer comes out in 24 hours",
                    "thumbnail":[{
                        "url":"https://images.heb.com/is/image/HEBGrocery/000402171"
                    }],
                    "fields" : [{
                        "name": "Name",
                        "value": time.name,
                    },
                    {
                        "name": "Location",
                        "value": time.location,
                    },
                    {
                        "name": "Stage",
                        "value": time.stage,
                        "inline": True
                    },
                    {
                        "name": "Type",
                        "value": time.type,
                        "inline": True
                    },
                    {
                        "name": "Owner",
                        "value": time.owner,
                        "inline": True
                    },
                    {
                        "name": "Timer",
                        "value": f'<t:{str(round(datetime.timestamp(time.time)))}:R>',
                        "inline": True
                    }
                ]
            }]
            request = requests.post(url, json=data)

