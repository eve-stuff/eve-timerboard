from django.core.management.base import BaseCommand
from django.db.models.functions import Now
from timerboard.models import Timer
from datetime import timedelta

class Command(BaseCommand):
    help = "Remove old timers"

    def handle(self, *args, **options):
        Timer._base_manager.filter(time__gte=Now()+timedelta(minutes=-15), time__lte=Now()).delete()