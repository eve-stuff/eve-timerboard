from django.core.exceptions import PermissionDenied
from timerboard.models import TimerBoardUser
from django.shortcuts import render, redirect

def user_is_verified(function):
    def wrap(request, *args ,**kwargs):
        verified = request.user.profile.account_verified()
        if verified:
            return function(request, *args, **kwargs)
        else:
            return render(request, 'timerboard/home.html')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

